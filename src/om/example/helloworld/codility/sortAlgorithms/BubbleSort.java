package om.example.helloworld.codility.sortAlgorithms;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] numArray = {2, 6, 4, 9, 12, 8};
        System.out.println("Before sort: " + Arrays.toString(numArray));
        System.out.println("After sort: " + Arrays.toString(bubbleSortMethod(numArray)));
        /*Before sort: [2, 6, 4, 9, 12, 8]
        After sort: [2, 4, 6, 8, 9, 12]*/
    }

    private static int[] bubbleSortMethod(int[] arr) {
        int temp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

}
