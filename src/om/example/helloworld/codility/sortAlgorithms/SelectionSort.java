package om.example.helloworld.codility.sortAlgorithms;

import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args) {
        int[] numArray = {4, 3, 14, 12, 8, 2, 1};
        System.out.println("Before sort: " + Arrays.toString(numArray));
        System.out.println("After sort: " + Arrays.toString(selectionSortAlgorithm(numArray)));
        /*Before sort: [4, 3, 14, 12, 8, 2, 1]
        After sort: [1, 2, 3, 4, 8, 12, 14]*/
    }


    private static int[] selectionSortAlgorithm(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
        return arr;
    }
}
