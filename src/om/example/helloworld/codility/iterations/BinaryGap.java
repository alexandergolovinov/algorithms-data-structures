package om.example.helloworld.codility.iterations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class BinaryGap {
    public static void main(String[] args) {
        findLongestBinaryGap(1041);
    }

    static public Integer findLongestBinaryGap(int binaryNumber) {
        BigInteger binaryNum = getBinaryFromInteger(binaryNumber);

        List<Integer> intervals = new ArrayList<>();
        String numberString = binaryNum.toString();
        int intervalLength = 1;

        for (int i = 0; i < numberString.length(); i++) {
            if (numberString.charAt(i) == '0' && i + 1 < numberString.length()) {
                if (numberString.charAt(i + 1) == '0') {
                    intervalLength++;
                } else if (numberString.charAt(i + 1) == '1') {
                    intervals.add(intervalLength);
                    intervalLength = 1;
                }
            }
        }
        Integer length = intervals.stream().mapToInt(v -> v).max().orElse(0);
        System.out.println("Length is: " + length);
        return length;
    }

    static public BigInteger getBinaryFromInteger(Integer number) {
        StringBuilder binaryNumberString = new StringBuilder();
        do {
            if (number % 2 == 0) {
                binaryNumberString.append(0);
            } else {
                binaryNumberString.append(1);
            }
            number = number / 2;
        } while (number != 0);
        binaryNumberString.reverse();
        System.out.println("Binary Number: " + binaryNumberString.toString());
        String binaryStringNumber = binaryNumberString.toString();
        try {
            return new BigInteger(binaryStringNumber.trim());
        } catch (NumberFormatException ex) {
            System.out.println("Error occured: " + ex.getMessage());
            return BigInteger.valueOf(0);
        }
    }
}
