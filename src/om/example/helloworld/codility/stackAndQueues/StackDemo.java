package om.example.helloworld.codility.stackAndQueues;

public class StackDemo {
    public static void main(String[] args) {
        Stack myStack = new Stack();
        myStack.push(5);

        System.out.println(myStack.isEmpty());
        myStack.pop();
        System.out.println(myStack.isEmpty());

    }

}
