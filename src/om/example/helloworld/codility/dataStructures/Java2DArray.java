package om.example.helloworld.codility.dataStructures;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Java2DArray {
    //https://www.hackerrank.com/challenges/java-2d-array/problem

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] arr = new int[6][6];

        for (int i = 0; i < 6; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 6; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }
        int maxHourglasses = calculateMaxHourglasses(arr);
        System.out.println(maxHourglasses);
        scanner.close();
    }

    private static int calculateMaxHourglasses(int[][] arr) {
        List<Integer> allSumsOfGlasses = new ArrayList<>();
        for (int i = 0; i < arr[0].length; i++) {
            if (i >= arr[0].length - 2) break;
            for (int j = 0; j < arr.length; j++) {
                if (j >= arr.length -2) break;
                int row1Val1 = arr[i][j];
                int row1Val2 = arr[i][j+1];
                int row1Val3 = arr[i][j+2];
                int row2Val1 = arr[i+1][j+1];
                int row3Val1 = arr[i+2][j];
                int row3Val2 = arr[i+2][j+1];
                int row3Val3 = arr[i+2][j+2];
                int sum = row1Val1 + row1Val2 + row1Val3 + row2Val1 + row3Val1 + row3Val2 + row3Val3;
                allSumsOfGlasses.add(sum);
            }
        }
        allSumsOfGlasses.sort(Collections.reverseOrder());

        return allSumsOfGlasses.get(0);
    }


}
