package om.example.helloworld.codility.dataStructures;

import java.util.Scanner;

public class JavaSubarray {
    //https://www.hackerrank.com/challenges/java-negative-subarray/problem

    public static void main(String[] args) {

        System.out.println("Insert the length of an array");
        Scanner scanner = new Scanner(System.in);
        int lengthOfArray = scanner.nextInt();
        int[] arr = new int[lengthOfArray];

        System.out.println("Insert elements to the array");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }

        int sumValue = 0;
        int total = 0;
        for (int i = 0; i < arr.length; i++) {
            int j = i; //next item
            while (j <= arr.length - 1) {
                sumValue = sumValue + arr[j];
                j++;
                printNegativeValue(sumValue);
                if (sumValue < 0 ) {
                    total++;
                }
            }
            //printNegativeValue(sumValue);
            sumValue = 0;
        }
        System.out.println("Total negative values " + total);
    }

    private static void printNegativeValue(int sum) {
        if (sum < 0) System.out.println(sum);
    }
}
