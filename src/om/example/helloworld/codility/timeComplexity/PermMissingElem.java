package om.example.helloworld.codility.timeComplexity;

public class PermMissingElem {
    public static void main(String[] args) {
        int[] data = new int[]{2, 3, 4, 5, 6, 7};
        solution(data);
    }

    public static int solution(int[] A) {
        //Find missing element from an array.
        //Loop over existing array incrementing number accordingly. if the number is not matching with current one, then it is missing
        if (A.length == 0) {
            return 1;
        }

        if (A[0] != 1) {
            return 1;
        }

        int counter = A[0];
        for (int i = 0; i < A.length; i++) {
            int currentNum = A[i];
            if (counter != currentNum) {
                System.out.println(counter);
                return counter;
            }
            counter++;
        }
        System.out.println(counter);
        return counter;
    }
}
