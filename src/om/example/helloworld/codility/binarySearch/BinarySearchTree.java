package om.example.helloworld.codility.binarySearch;

/**
 * BinarySearchTree stores the lower value in the left node and higher data in the right node
 */
public class BinarySearchTree {
    /**
     * BinaryTree has only reference to the root node
     */
    private TreeNode root;

    public void insert(Integer data) {
        if (root != null) {
            root.insert(data);
        }
    }

    public TreeNode find(Integer data) {
        if (root != null) {
            root.find(data);
        }
        return null;
    }

    public Integer findSmallest() {
        if (this.root != null) {
            return root.findSmallest();
        }
        return null;
    }

    public Integer findLargest() {
        if (this.root != null) {
            return root.findLargest();
        }
        return null;
    }

    /**
     * Deleting Node. Soft Delete. Marking Node as Deleted.
     */
    public void delete(Integer data) {
        TreeNode toDelete = find(data);
        toDelete.delete();
    }

    /**
     * Deleting Node. Removing actual Node
     */
    /*public void delete(Integer data) {
        TreeNode current = this.root;
        TreeNode parent = this.root;
        boolean isLeftChild = false;

        if (current == null) return;

        while (current != null && current.getData() != data) {
            parent = current;

            if (data < current.getData()) {
                current = current.getLeftChild();
                isLeftChild = true;
            } else {
                current = current.getRightChild();
                isLeftChild = false;
            }

        }
        if (current == null) return;

        if (current.getLeftChild() == null && current.getRightChild() == null) {
            if (current == root) {
                root = null;
            } else {
                if (isLeftChild)
                    parent.setLeftChild(null);
                else
                    parent.setRightChild(null);
            }
            //USING Left Node as a new Node for the LEFT tree
        } else if (current.getRightChild() == null) {
            if (current == root) {
                root = current.getLeftChild();
            } else if (isLeftChild) {
                parent.setLeftChild(current.getLeftChild());
            } else {
                parent.setRightChild(current.getLeftChild());
            }
            //USING Right Node as a new Node for the RIGHT tree
        } else if (current.getLeftChild() == null) {
            if (current == root) {
                root = current.getRightChild();
            } else if (isLeftChild) {
                parent.setLeftChild(current.getRightChild());
            } else {
                parent.setRightChild(current.getRightChild());
            }
        }
    }*/
}
