package om.example.helloworld.codility.binarySearch;

public class TreeNode {

    private Integer data;
    private TreeNode leftChild;
    private TreeNode rightChild;
    //Soft Delete Approach. Instead of doing complicated delete operation, we just mark the node as Deleted.
    private boolean isDeleted;

    public TreeNode(Integer data) {
        this.data = data;
    }

    public void insert(Integer data) {
        if (data >= this.data) {
            if (rightChild == null) {
                setRightChild(new TreeNode(data));
            } else {
                rightChild.insert(data);
            }
        } else {
            if (leftChild == null) {
                setLeftChild(new TreeNode(data));
            } else {
                leftChild.insert(data);
            }
        }
    }

    public Integer findSmallest() {
        if (this.leftChild == null) {
            return this.data;
        }
        return this.leftChild.findSmallest();
    }

    public Integer findLargest() {
        if (this.rightChild == null) {
            return this.data;
        }
        return this.rightChild.findLargest();
    }

    public TreeNode find(Integer data) {
        if (this.data == data && !isDeleted) {
            return this;
        }
        //Recursive Approach
        if (data < this.data && leftChild != null) {
            return leftChild.find(data);
        }

        if (data > this.data && rightChild != null) {
            return rightChild.find(data);
        }
        //No treeNode was found containing the *data*, so return null
        return null;
    }

    public void delete() {
        this.isDeleted = true;
    }

    public Integer getData() {
        return data;
    }

    public TreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }
}
