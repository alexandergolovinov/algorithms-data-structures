package om.example.helloworld.codility.linkedList;

public class DoubleLinkedList {
    private DoubleNode head;
    private DoubleNode tail;

    public void insertAtHead(int data) {
        DoubleNode newNode = new DoubleNode(data);
        newNode.setNextNode(this.head);
        if (this.head != null) {
            this.head.setPreviousNode(newNode);
        }
        this.head = newNode;

    }


}
