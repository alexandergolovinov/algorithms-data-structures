package om.example.helloworld.codility.linkedList;

public class DoublyEndedList {
    private Node head;
    private Node tail;

    public void insertAtTail(int data) {
        Node newNode = new Node(data);
        if (this.head == null) {
            this.head = newNode;
        }

        if (this.tail != null) {
            this.tail.setNextNode(newNode);
        }
        this.tail = newNode;
    }

    @Override
    public String toString() {
        Node current = this.head;
        StringBuilder result = new StringBuilder();
        while (current != null) {
            result.append(current.toString());
            current = current.getNextNode();
        }
        return result.toString();
    }

}
