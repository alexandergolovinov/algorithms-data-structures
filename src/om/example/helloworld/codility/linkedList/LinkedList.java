package om.example.helloworld.codility.linkedList;

public class LinkedList {

    private Node head;

    public void insertAtHead(int data) {
        Node newNode = new Node(data);
        newNode.setNextNode(this.head);
        this.head = newNode;
    }

    /**
     * Once your make head point to the next element, then the first element is ready for garbage collection and will be removed from memory.
     */
    public void deleteAtHead() {
        this.head = this.head.getNextNode();
    }

    public Node find(int data) {
        Node current = this.head;
        while (current != null) {
            if (current.getData() == data) return current;
            current = current.getNextNode();
        }
        return null;
    }

    public int length() {
        int length = 0;
        Node current = this.head;
        while (current != null) {
            length++;
            current = current.getNextNode();
        }
        return length;
    }

    @Override
    public String toString() {
        Node current = this.head;
        StringBuilder result = new StringBuilder();
        while (current != null) {
            result.append(current.toString());
            current = current.getNextNode();
        }
        return result.toString();
    }
}
