package om.example.helloworld.codility.linkedList;

public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.insertAtHead(5);
        list.insertAtHead(10);
        list.insertAtHead(2);
        list.insertAtHead(12);
        list.insertAtHead(19);

        System.out.println(list.toString()); // Data: 19 Data: 12 Data: 2 Data: 10 Data: 5
        System.out.println("Length is: " + list.length()); //Length is: 5

        list.deleteAtHead();

        System.out.println("Found: " + list.find(5)); //Found: 5
        System.out.println("Found: " + list.find(11)); //Found: null


    }
}
