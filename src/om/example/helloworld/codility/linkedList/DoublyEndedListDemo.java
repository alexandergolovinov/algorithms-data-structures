package om.example.helloworld.codility.linkedList;

public class DoublyEndedListDemo {
    public static void main(String[] args) {
        DoublyEndedList dList = new DoublyEndedList();
        dList.insertAtTail(19);
        dList.insertAtTail(12);
        dList.insertAtTail(8);
        System.out.println(dList);
    }
}
