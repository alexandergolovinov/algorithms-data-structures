package om.example.helloworld.codility.linkedList;

public class DoubleNode {
    private int data;
    private DoubleNode nextNode;
    private DoubleNode previousNode;

    public DoubleNode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public DoubleNode getNextNode() {
        return nextNode;
    }

    public void setNextNode(DoubleNode nextNode) {
        this.nextNode = nextNode;
    }

    public DoubleNode getPreviousNode() {
        return previousNode;
    }

    public void setPreviousNode(DoubleNode previousNode) {
        this.previousNode = previousNode;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
