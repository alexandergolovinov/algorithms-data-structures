package om.example.helloworld.codility.recursion;

public class TailRecursion {

    /**
     *
     * @param n
     * @param result accumulator
     * @return
     */
    public int factorial (int n, int result) {

        if(n==0) return result;
        return factorial(n-1, n * result);

    }
}
