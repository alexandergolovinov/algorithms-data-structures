package om.example.helloworld.codility.arrays;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CyclicRotation {
    public static void main(String[] args) {
        int[] example = new int[]{1, 2, 7, 4};
        solution(example, 2);
    }

    public static int[] solution(int[] A, int K) {
        List<Integer> arrayList = Arrays.stream(A).boxed().collect(Collectors.toList());
        Collections.rotate(arrayList, K);
        arrayList.forEach(System.out::println);
        return arrayList.stream().mapToInt(i -> i).toArray();
    }
}
