package om.example.helloworld.codility.arrays;

import java.util.HashMap;
import java.util.Map;

public class OddOccurrencesInArray {

    public static void main(String[] args) {
            int[] A = new int[]{9, 3, 9, 3, 9, 7, 9};
            solution(A);
    }

    public static int solution(int[] A) {
        HashMap<Integer, Integer> hashMapOccurencies = new HashMap<>(A.length);

        for (Integer el: A) {
            Integer occurency = hashMapOccurencies.get(el);
            hashMapOccurencies.put(el, occurency == null ? 1 : occurency + 1);
        }

        for (Map.Entry<Integer, Integer> e : hashMapOccurencies.entrySet()) {
            if (e.getValue() == 1 || e.getValue() % 2 != 0) {
                System.out.println(e.getKey());
                return e.getKey();
            }
        }
        System.out.println("No Occurencies");
        return -1;
    };
}
